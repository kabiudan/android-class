package com.example.sefu.screenapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ScreenOne extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_one);
    }

    public void backToScreenMain(View view) {
        Intent screeOne = new Intent(getApplicationContext(), MainScreen.class);
        startActivity(screeOne);

    }
}
